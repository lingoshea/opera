#include <fs_related/fs_bitmap.h>
#include <sys_related/opera_error.h>
#include <exception>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <cstring>

int main()
{
    try
    {
        unlink("disk.img");

        fd_t file = open("disk.img", O_RDWR | O_CREAT);
        if (file == -1)
        {
            str_throw_hmnx_error("Cannot open img file!");
        }

        if (ftruncate(file, BLOCK_SIZE * 32) == -1)
        {
            str_throw_hmnx_error("Cannot truncate!");
        }

        if (fchmod(file, 0666) == -1)
        {
            str_throw_hmnx_error("Cannot chmod!");
        }

        close(file);

        device dev;
        dev.init("disk.img", 10);

        fs_bitmap_t bitmap{};
        bitmap.init(&dev, 1, 1, 4096, 0);

        for (length_t i = 0; i < 4096; i++)
        {
            bitmap.set_status(i, true);
        }

        for (length_t i = 0; i < 4096; i++)
        {
            if (!bitmap.get_status(i))
            {
                str_throw_hmnx_error("Test failed");
            }
        }

        for (length_t i = 0; i < 4096; i++)
        {
            bitmap.set_status(i, false);
        }

        for (length_t i = 0; i < 4096; i++)
        {
            if (bitmap.get_status(i))
            {
                str_throw_hmnx_error("Test failed");
            }
        }

        for (length_t i = 0; i < 4096; i++)
        {
            bitmap.set_status(i, false);
            if (bitmap.get_status(i))
            {
                str_throw_hmnx_error("Test failed");
            }
            bitmap.set_status(i, true);
            if (!bitmap.get_status(i))
            {
                str_throw_hmnx_error("Test failed");
            }
        }

        for (length_t i = 0; i < 4093; i++)
        {
            bitmap.set_status(i, false);
        }

        bitmap.set_status(1023, true);

        try {
            for (length_t i = 0; i < 4098; i++) {
                std::cout << bitmap.get_free_node() << std::endl;
            }
        } catch (opera_error_t & err)
        {
            if (err.get_xerrcode() == ERROR_LOGIC_BLOCK_DEPLETED)
            {
                std::cout << err.what() << std::endl;
                goto _continue;
            }

            throw ;
        }

        _continue:
        for (length_t i = 0; i < 4096; i++)
        {
            if (!bitmap.get_status(i))
            {
                str_throw_hmnx_error("Test failed");
            }
        }

        return EXIT_SUCCESS;
    }
    catch (opera_error_t & err)
    {
        std::cout << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}

