#include <sys_related/device.h>
#include <sys_related/opera_error.h>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <cstring>

int main()
{
    try
    {
        unlink("disk.img");

        fd_t file = open("disk.img", O_RDWR | O_CREAT);
        if (file == -1)
        {
            str_throw_hmnx_error("Cannot open img file!");
        }

        if (ftruncate(file, BLOCK_SIZE * 32) == -1)
        {
            str_throw_hmnx_error("Cannot truncate!");
        }

        if (fchmod(file, 0666) == -1)
        {
            str_throw_hmnx_error("Cannot chmod!");
        }

        close(file);


        char cbuf[256] {};
        device dev;
        dev.init("disk.img", 10);

        for (uint64_t i = 0; i < 15; i++)
        {
            auto & buff = dev.alloc_bbuf(i);
            buff.write("MX250", 5, 0);
            buff.read(cbuf, 10, 0);
            if (!!memcmp(cbuf, "MX250", 5))
            {
                str_throw_hmnx_error("Buffer I/O is buggy");
            }
            buff.drop();
        }

        {
            auto & buff = dev.alloc_bbuf(16);
            buff.write("MX250", 5, 0);
            buff.drop();
        }

        {
            auto & buff = dev.alloc_bbuf(13);
            buff.write("MX250", 5, 0);
        }

        {
            auto & buff = dev.alloc_bbuf(18);
            buff.write("MX250", 5, 0);
            buff.drop();
        }

        dev.sync();

        return EXIT_SUCCESS;
    }
    catch (opera_error_t & err)
    {
        std::cout << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}
