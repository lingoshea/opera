#include <fs_related/fs_logic_block.h>
#include <sys_related/opera_error.h>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

int main()
{
    try
    {
        unlink("disk.img");

        fd_t file = open("disk.img", O_RDWR | O_CREAT);
        if (file == -1)
        {
            str_throw_hmnx_error("Cannot open img file!");
        }

        if (ftruncate(file, BLOCK_SIZE * 32) == -1)
        {
            str_throw_hmnx_error("Cannot truncate!");
        }

        if (fchmod(file, 0666) == -1)
        {
            str_throw_hmnx_error("Cannot chmod!");
        }

        close(file);


        device dev;
        dev.init("disk.img", 10);

        logic_block lblock;
        lblock.init(&dev, 0, 1, 0, 1, 4096);

        try
        {
            for (uint64_t i = 0; i < 5000; i++)
            {
                lblock.get_free_logic_block();
            }
        } catch (opera_error_t & err)
        {
            if (err.get_xerrcode() == ERROR_LOGIC_BLOCK_DEPLETED)
            {
                std::cout << err.what() << std::endl;
                goto _continue;
            }

            throw;
        }

        _continue:
        for (uint64_t i = 0; i < 4096; i++)
        {
            lblock.delete_block(i + 1);
        }

        for (uint64_t i = 0; i < 4096; i++)
        {
            std::cout << lblock.get_free_logic_block() << std::endl;
        }

        std::cout << lblock.get_last_allocated_block_number() << std::endl;

        return EXIT_SUCCESS;
    }
    catch (opera_error_t & err)
    {
        std::cout << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}
