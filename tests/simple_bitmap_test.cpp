#include <sys_related/simple_bitmap.h>
#include <sys_related/opera_error.h>
#include <exception>
#include <iostream>

int main()
{
    try {
        simple_bitmap bitmap;
        bitmap.init(10);

        for (length_t i = 0; i < 10; i++)
        {
            std::cout << bitmap.get_free_node() << std::endl;
        }

        for (uint64_t i = 0; i < 1000; i++)
        {
            for (uint64_t j = 0; j < 10; j++)
            {
                bitmap.set_status_of(j, false);
                bitmap.set_status_of(j, true);
                bitmap.set_status_of(j, false);
            }
        }

        for (length_t i = 0; i < 10; i++)
        {
            std::cout << bitmap.get_free_node() << std::endl;
        }

        bitmap.cleanup();
        return EXIT_FAILURE;
    }
    catch (opera_error_t & err)
    {
        std::cout << err.what() << std::endl;
        if (err.get_xerrcode() == ERROR_BUFFER_DEPLETED)
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}
