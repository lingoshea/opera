#include <fuse_related/inode.h>
#include <sys_related/opera_error.h>
#include <exception>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <cstring>

int main()
{
    try
    {
        unlink("disk.img");

        fd_t file = open("disk.img", O_RDWR | O_CREAT);
        if (file == -1)
        {
            str_throw_hmnx_error("Cannot open img file!");
        }

        if (ftruncate(file, BLOCK_SIZE * 64) == -1)
        {
            str_throw_hmnx_error("Cannot truncate!");
        }

        if (fchmod(file, 0666) == -1)
        {
            str_throw_hmnx_error("Cannot chmod!");
        }

        close(file);

        device dev;
        dev.init("disk.img", 10);

        logic_block filesystem_logic_blocks { };
        filesystem_logic_blocks.init(&dev,
                                     0,
                                     1,
                                     0,
                                     1,
                                     4096);

        filesystem_logic_blocks.bitmap.set_status(0, true);

        if (!filesystem_logic_blocks.bitmap.get_status(0))
        {
            str_throw_hmnx_error("Test failed");
        }

        opera_inode_t root_inode
                {
                        .links = 2,
                        .mode = 0660,
                        .file_len = 0,
                        .gid = 0,
                        .uid = 0,
                        .access_time = current_time(),
                        .change_time = current_time(),
                        .modify_time = current_time()
                };

        auto & inode_buff = dev.alloc_bbuf(1);
        inode_buff.write((char*)&root_inode, sizeof (root_inode), 0);
        inode_buff.drop();

        inode my_inode;
        my_inode.init(&filesystem_logic_blocks, 1);

        for (uint64_t i = 0; i < 4 * BLOCK_SIZE; i++)
        {
            my_inode.write((char*)&i, sizeof (i), i * sizeof(i));
            std::cout << "Write 8 " << i << std::endl;
        }

        for (uint64_t i = 0; i < 4 * BLOCK_SIZE; i++)
        {
            uint64_t tmp;
            my_inode.read((char*)&tmp, sizeof (tmp), i * sizeof(tmp));

            std::cout << "Read 8 " << i << " ->" << tmp << std::endl;

//            if (tmp != i)
//            {
//                str_throw_hmnx_error("Test failed");
//            }
        }

        return EXIT_SUCCESS;
    }
    catch (opera_error_t & err)
    {
        std::cout << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}

