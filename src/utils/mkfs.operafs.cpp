#include <getopt.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <stdafx.h>
#include <sys_related/device.h>
#include <opera.h>
#include <fs_related/fs_bitmap.h>

gid_t       format_gid = 0;
uid_t       format_uid = 0;
char *      device_name         = nullptr;
#define VERSION "mkfs.operafs " GENERAL_VERSION


void help(char ** argv)
{
    fprintf(stdout,
            VERSION "\n"
                    "Usage: %s [OPTIONS] DEVICE\n"
                    "       -h, --help          Show this help message.\n"
                    "       -k, --keep          Use the same GID and UID as current user..\n"
                    , *argv);
}

void phrase_opt(int argc, char ** argv)
{
    int opt;
    int option_index;

    static struct option long_options[] =
            {
                    {"help",        no_argument,       nullptr, 'h'},
                    {nullptr, 0, nullptr, 0}
            };

    while ((opt = getopt_long(argc, argv, "hk", long_options, &option_index)) != -1)
    {
        switch (opt)
        {
            case 'h':
                help(argv);
                exit(EXIT_SUCCESS);

            case 'k':
                std::cout << "Keep UID and GID" << std::endl;
                format_uid = getuid();
                format_gid = getgid();
                break;

            case '?':
                /* getopt_long already printed an error message. */
            default: /* '?' */
                help(argv);
                exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc)
    {
        fprintf(stderr, "Missing path to device after options.\n");
        help(argv);
        exit(EXIT_FAILURE);
    }

    device_name = argv[optind];
}

int main(int argc, char ** argv)
{
    try {

        phrase_opt(argc, argv);

        device file;
        file.init(device_name, 512);

        length_t block_count = file.block_count() - 1;
        length_t bitmap_block_count = block_count / (8 * BLOCK_SIZE + 1);
        if (bitmap_block_count == 0)
        {
            bitmap_block_count = 1;
        }

        length_t logic_block_count = block_count - bitmap_block_count;

        // save super block
        opera_super_block_t super
                {
                        .magic = FILESYSTEM_MAGIC_NUMBER,
                        .bitmap_block_count = bitmap_block_count,
                        .logic_block_count = logic_block_count,
                        .free_logic_block_count = logic_block_count - 2,
                        .last_allocated_logic_number = 2, // 1 and 2 is used
                        .mounted = 0,
                        .last_mount_time = current_time(),
                        .magic_cmp = ~FILESYSTEM_MAGIC_NUMBER
                };
        auto &buff = file.alloc_bbuf(0);
        buff.write((char *) &super, sizeof(super), 0);
        buff.drop();

        std::cout << "Filesystem filed block count: " << block_count << std::endl;

        // clear bitmap
        std::cout << "Clearing bitmap (" << super.bitmap_block_count << " blocks)..." << std::endl;
        char empty_block[BLOCK_SIZE]{};
        memset(empty_block, 0, sizeof (empty_block));
        for (length_t i = 1; i <= bitmap_block_count; i++)
        {
            auto &bit_buff = file.alloc_bbuf(i);
            bit_buff.write(empty_block, sizeof(empty_block), 0);
            bit_buff.drop();
        }

        // mark root blocks
        fs_bitmap_t bitmap;
        bitmap.init(&file,
                    1,
                    bitmap_block_count,
                    logic_block_count,
                    0);
        bitmap.set_status(0, true);
        bitmap.set_status(1, true);

        std::cout << "Logic block count: " << super.logic_block_count << std::endl;

        std::cout << "Constructing root inode ..." << std::endl;

        opera_inode_t root_inode
                {
                    .links = 2,
                    .mode = S_IFDIR | 0755,
                    .file_len = sizeof(opera_dentry_t) * 2,
                    .gid = format_gid,
                    .uid = format_uid,
                    .access_time = current_time(),
                    .change_time = current_time(),
                    .modify_time = current_time()
                };
        root_inode.block_filed[0] = 2;

        opera_dentry_t root_dentry[2];
        strcpy(root_dentry[0].filename, ".");
        strcpy(root_dentry[1].filename, "..");
        root_dentry[0].inode_logic_block_num = 1;
        root_dentry[1].inode_logic_block_num = 1;

        auto & inode_buf = file.alloc_bbuf(bitmap_block_count + 1);
        inode_buf.write((char*)&root_inode, sizeof (root_inode), 0);
        inode_buf.drop();

        auto & dentry_buf = file.alloc_bbuf(bitmap_block_count + 2);
        dentry_buf.write((char*)&root_dentry, sizeof (opera_dentry_t) * 2, 0);
        dentry_buf.drop();

        std::cout << "Sync filsystem ..." << std::endl;

        file.cleanup();

        std::cout << "Filesystem formatted" << std::endl;

        return EXIT_SUCCESS;
    }
    catch (std::exception & err)
    {
        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}
