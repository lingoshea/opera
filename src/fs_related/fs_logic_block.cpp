#include <fs_related/fs_logic_block.h>

void logic_block::init(device * _file,
                         length_t _bitmap_starting_block,
                         length_t _bitmap_block_count,
                         length_t _last_allocated_node,
                         length_t _logic_block_starting_block,
                         length_t _logic_block_count)
{
    file = _file;
    bitmap.init(_file,
           _bitmap_starting_block,
           _bitmap_block_count,
           _logic_block_count,
           _last_allocated_node),
    logic_block_starting_block = _logic_block_starting_block;
    logic_block_count = _logic_block_count;
}

buffer &logic_block::access(length_t logic_block_number)
{
    return file->alloc_bbuf(logic_block_starting_block + logic_block_number - 1);
}

length_t logic_block::get_free_logic_block()
{
    return bitmap.get_free_node() + 1;
}

void logic_block::delete_block(length_t logic_block_number)
{
    bitmap.set_status(logic_block_number - 1, false);
}
