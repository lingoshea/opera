#include <fs_related/fs_bitmap.h>
#include <sys_related/opera_error.h>

void fs_bitmap_t::init(device * _file,
                               length_t _bitmap_starting_block,
                               length_t _bitmap_block_count,
                               length_t _bitmap_size,
                               length_t _last_allocated_node)
{
    file = _file;
    bitmap_block_count = _bitmap_block_count;
    bitmap_starting_block = _bitmap_starting_block;
    bitmap_size = _bitmap_size;
    last_allocated_node = _last_allocated_node;
}

bool fs_bitmap_t::get_status(length_t bit_offset)
{
    if (bit_offset >= bitmap_size)
    {
        return false;
    }

    length_t bit_in_byte_pos = bit_offset / 8;
    length_t bit_in_byte_off = bit_offset % 8;
    length_t bit_block_pos = bit_in_byte_pos / (BLOCK_SIZE);
    length_t bit_in_block_offset = bit_in_byte_pos % (BLOCK_SIZE);
    uint8_t bit, comp = (0x01 << bit_in_byte_off);

    auto & buff = file->alloc_bbuf(bit_block_pos + bitmap_starting_block);
    buff.read((char*)&bit, 1, bit_in_block_offset);
    buff.drop();

    return comp & bit;
}

void fs_bitmap_t::set_status(length_t bit_offset, bool status)
{
    if (bit_offset >= bitmap_size)
    {
        return;
    }

    length_t bit_in_byte_pos = bit_offset / 8;
    length_t bit_in_byte_off = bit_offset % 8;
    length_t bit_block_pos = bit_in_byte_pos / (BLOCK_SIZE);
    length_t bit_in_block_offset = bit_in_byte_pos % (BLOCK_SIZE);
    uint8_t bit, comp = (0x01 << bit_in_byte_off);
    comp = ~comp;

    auto & buff = file->alloc_bbuf(bit_block_pos + bitmap_starting_block);
    buff.read((char*)&bit, 1, bit_in_block_offset);

    bit &= comp; // delete bit

    if (status) // set bit
    {
        bit |= ~comp;
    }

    buff.write((char*)&bit, 1, bit_in_block_offset);
    buff.drop();
}

length_t fs_bitmap_t::get_free_node()
{
    if (last_allocated_node + 1 == bitmap_size)
    {
        last_allocated_node = 0;
    }

    for (length_t i = last_allocated_node; i < bitmap_size; i++)
    {
        if (!get_status(i))
        {
            last_allocated_node = i;
            set_status(i, true);
            return i;
        }
    }

    for (length_t i = 0; i < bitmap_size; i++)
    {
        if (!get_status(i))
        {
            last_allocated_node = i;
            set_status(i, true);
            return i;
        }
    }

    code_throw_hmnx_error(ERROR_LOGIC_BLOCK_DEPLETED);
}
