#include <stdafx.h>
#include <chrono>

bool debug = true;

struct timespec current_time()
{
    struct timespec ts{};
    timespec_get(&ts, TIME_UTC);
    return ts;
}

