#include <fuse_related/operations.h>
#include <fuse_related/inode.h>
#include <sys_related/opera_error.h>
#include <cstring>

device      opera::filesystem_device;
logic_block opera::filesystem_logic_blocks;

length_t opera::namei(path_t pathname)
{
    if (pathname.depth() == 0)
    {
        return ROOT_INODE_LOGIC_BLOCK_RESIDENCE;
    }

    length_t inode_num = ROOT_INODE_LOGIC_BLOCK_RESIDENCE;
    inode _inode;
    _inode.init(&opera::filesystem_logic_blocks, ROOT_INODE_LOGIC_BLOCK_RESIDENCE);

    for (auto & name : pathname)
    {
        inode_num = opera::find_entry_in_dir(_inode, name);
        _inode.init(&opera::filesystem_logic_blocks, inode_num);
    }

    return inode_num;
}

void opera::mknod(path_t pathname, unsigned int mode, gid_t gid, uid_t uid, dev_t device)
{
    if (pathname.depth() == 0)
    {
        return;
    }

    std::string target_name = pathname.pop();

    // fill in information
    opera_inode_t _inode_info
            {
                    .links = 1,
                    .mode = mode,
                    .file_len = 0,
                    .gid = gid,
                    .uid = uid,
                    .access_time = current_time(),
                    .change_time = current_time(),
                    .modify_time = current_time()
            };
    opera_dentry_t dentry
    {
        .inode_logic_block_num = opera::filesystem_logic_blocks.get_free_logic_block()
    };
    strncpy(dentry.filename, target_name.c_str(), sizeof (dentry.filename));

    if (device != 0)
    {
        _inode_info.block_filed[0] = device;
    }

    // save dentry information
    auto parent_inode_block_number = namei(pathname);
    inode _parent_inode;
    _parent_inode.init(&opera::filesystem_logic_blocks,
                        parent_inode_block_number);
    _parent_inode.write((char*)&dentry, sizeof (dentry), _parent_inode.get_file_len());

    // save inode information
    auto & target_inode_buff = opera::filesystem_logic_blocks.access(dentry.inode_logic_block_num);
    target_inode_buff.write((char*)&_inode_info, sizeof (_inode_info), 0);
    target_inode_buff.drop();
}

void opera::unlink(const path_t& pathname)
{
    if (pathname.depth() == 0)
    {
        return;
    }

    auto target_inode_num = opera::namei(pathname);
    inode target_inode;
    target_inode.init(&opera::filesystem_logic_blocks, target_inode_num);
    auto _inode_info = target_inode.get_inode();
    _inode_info.links -= 1;
    target_inode.save_inode(_inode_info);

    // delete inode if no dentry links
    if (_inode_info.links == 0)
    {
        opera::filesystem_logic_blocks.delete_block(target_inode_num);
    }

    // remove dentry from dir
    opera::rmdentry(pathname);
}

void opera::rmdentry(path_t pathname)
{
    auto target_name = pathname.pop();
    auto parent_inode_num = opera::namei(pathname);
    inode parent_inode;
    parent_inode.init(&opera::filesystem_logic_blocks, parent_inode_num);
    auto dentry_count = parent_inode.get_file_len() / sizeof (opera_dentry_t);
    opera_dentry_t dentry;

    for (length_t i = 0; i < dentry_count; i++)
    {
        parent_inode.read((char*)&dentry, sizeof (dentry), i * sizeof(dentry));
        if (!strncmp(target_name.c_str(), dentry.filename, sizeof(dentry.filename)))
        {
            memset(&dentry, 0, sizeof(dentry));
            parent_inode.write((char*)&dentry, sizeof (dentry), i * sizeof(dentry));
            return;
        }
    }

    errno = ENOENT;
    code_throw_hmnx_error(ERROR_ENTRY_NOT_FOUND);
}

length_t opera::find_entry_in_dir(inode & _inode,
                                  const std::string& entry)
{
    opera_dentry_t dentry;
    auto dentry_count = _inode.get_file_len() / sizeof (opera_dentry_t);
    for (uint64_t i = 0; i < dentry_count; i++)
    {
        _inode.read((char*)&dentry, sizeof (dentry), i * sizeof (dentry));
        if (!strncmp(dentry.filename, entry.c_str(), sizeof (dentry.filename)))
        {
            return dentry.inode_logic_block_num;
        }
    }

    errno = ENOENT;
    code_throw_hmnx_error(ERROR_ENTRY_NOT_FOUND);
}
