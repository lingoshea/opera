#include <fuse_related/inode.h>
#include <sys_related/opera_error.h>
#define MIN(a, b) ((a) < (b) ? (a) : (b))

void inode::init(logic_block * _logic_blocks, length_t _inode_logic_block_number)
{
    logic_blocks = _logic_blocks;
    inode_logic_block_number = _inode_logic_block_number;
}

length_t inode::read(char * output_buffer, length_t length, length_t offset)
{
    if (length + offset > FIRST_LEVEL_LENGTH)
    {
        errno = ENOSYS;
        code_throw_hmnx_error(ERROR_NO_IMPLEMENTATION);
    }

    if (offset > FIRST_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + THIRD_LEVEL_LENGTH)
    {
        return 0;
    }

    if (offset + length > FIRST_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + THIRD_LEVEL_LENGTH)
    {
        length = FIRST_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + THIRD_LEVEL_LENGTH - offset;
    }

    length_t read_len;
    length_t end_point = offset + length;

    // read starts in 1st level and stops in 1st level
    if (offset < FIRST_LEVEL_LENGTH && end_point <= FIRST_LEVEL_LENGTH)
    {
        return read_in_1st_level(output_buffer, length, offset);
    }

    // read starts in 1st level and stops in 2nd level
    if (offset < FIRST_LEVEL_LENGTH && end_point > FIRST_LEVEL_LENGTH
    && end_point <= (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        read_len = read_in_1st_level(output_buffer, FIRST_LEVEL_LENGTH - offset, offset);
        read_len += read_in_2nd_level(output_buffer + read_len, end_point - FIRST_LEVEL_LENGTH, 0);
        return read_len;
    }

    // read starts in 1st level and stops in 3rd level
    if (offset < FIRST_LEVEL_LENGTH && end_point > (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
//    && end_point <= (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        read_len = read_in_1st_level(output_buffer, FIRST_LEVEL_LENGTH - offset, offset);
        read_len += read_in_2nd_level(output_buffer + read_len, SECOND_LEVEL_LENGTH, 0);
        read_len += read_in_3rd_level(output_buffer + read_len,
                                      end_point - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH),
                                      0);
        return read_len;
    }

    // read starts in 2nd level and stops in 2nd level
    if (offset > FIRST_LEVEL_LENGTH && offset < (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH)
        && end_point <= (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        return read_in_2nd_level(output_buffer,
                                 end_point - FIRST_LEVEL_LENGTH,
                                 offset - FIRST_LEVEL_LENGTH);
    }

    // read starts in 2nd level and stops in 3rd level
    if (offset > FIRST_LEVEL_LENGTH && offset < (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH)
        && end_point > (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
//    && end_point <= (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        read_len = read_in_2nd_level(output_buffer,
                                     (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH) - offset,
                                     offset - FIRST_LEVEL_LENGTH);
        read_len += read_in_3rd_level(output_buffer + read_len,
                                      end_point - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH),
                                      0);
        return read_len;
    }

    // read starts in 3rd level and stops in 3rd level
    if (offset > (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH)
        && offset < (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
//        && end_point <= (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        return read_in_3rd_level(output_buffer,
                                 end_point - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH),
                                 offset - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH));
    }

    return 0;
}

length_t inode::write(const char * input_buffer, length_t length, length_t offset)
{
    if (length + offset > FIRST_LEVEL_LENGTH)
    {
        errno = ENOSYS;
        code_throw_hmnx_error(ERROR_NO_IMPLEMENTATION);
    }

    if (offset > FIRST_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + THIRD_LEVEL_LENGTH)
    {
        return 0;
    }

    if (offset + length > FIRST_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + THIRD_LEVEL_LENGTH)
    {
        length = FIRST_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + THIRD_LEVEL_LENGTH - offset;
    }

    length_t write_len;
    length_t end_point = offset + length;

    // resize
    i_truncate(end_point);

    // write starts in 1st level and stops in 1st level
    if (offset < FIRST_LEVEL_LENGTH && end_point <= FIRST_LEVEL_LENGTH)
    {
        return write_in_1st_level(input_buffer, length, offset);
    }

    // write starts in 1st level and stops in 2nd level
    if (offset < FIRST_LEVEL_LENGTH && end_point > FIRST_LEVEL_LENGTH
        && end_point <= (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        write_len = write_in_1st_level(input_buffer, FIRST_LEVEL_LENGTH - offset, offset);
        write_len += write_in_2nd_level(input_buffer + write_len,
                                        end_point - FIRST_LEVEL_LENGTH, 0);
        return write_len;
    }

    // write starts in 1st level and stops in 3rd level
    if (offset < FIRST_LEVEL_LENGTH && end_point > (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
//    && end_point <= (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        write_len = write_in_1st_level(input_buffer, FIRST_LEVEL_LENGTH - offset, offset);
        write_len += write_in_2nd_level(input_buffer + write_len, SECOND_LEVEL_LENGTH, 0);
        write_len += write_in_3rd_level(input_buffer + write_len,
                                      end_point - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH),
                                      0);
        return write_len;
    }

    // write starts in 2nd level and stops in 2nd level
    if (offset > FIRST_LEVEL_LENGTH && offset < (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH)
        && end_point <= (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        return write_in_2nd_level(input_buffer,
                                 end_point - FIRST_LEVEL_LENGTH,
                                 offset - FIRST_LEVEL_LENGTH);
    }

    // write starts in 2nd level and stops in 3rd level
    if (offset > FIRST_LEVEL_LENGTH && offset < (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH)
                                                && end_point > (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
//    && end_point <= (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        write_len = write_in_2nd_level(input_buffer,
                                     (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH) - offset,
                                     offset - FIRST_LEVEL_LENGTH);
        write_len += write_in_3rd_level(input_buffer + write_len,
                                      end_point - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH),
                                      0);
        return write_len;
    }

    // write starts in 3rd level and stops in 3rd level
    if (offset > (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH)
        && offset < (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
//        && end_point <= (THIRD_LEVEL_LENGTH + SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH))
    {
        return write_in_3rd_level(input_buffer,
                                 end_point - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH),
                                 offset - (SECOND_LEVEL_LENGTH + FIRST_LEVEL_LENGTH));
    }

    return 0;
}

length_t inode::read_in_1st_level(char * output_buffer, length_t length, length_t offset)
{
    length_t read_len;
    length_t starting_block = offset / BLOCK_SIZE;
    length_t starting_block_off = offset % BLOCK_SIZE;
    length_t starting_block_read_len = MIN(BLOCK_SIZE - starting_block_off, length);
    length_t remaining_size = length - starting_block_read_len;
    length_t full_block_count = remaining_size / BLOCK_SIZE;
    length_t last_block_read_len = remaining_size > BLOCK_SIZE ? remaining_size % BLOCK_SIZE : 0;
    auto _inode = get_inode();

    auto & starting_buff = logic_blocks->access(_inode.block_filed[starting_block]);
    read_len = starting_buff.read(output_buffer, starting_block_read_len, starting_block_off);
    starting_buff.drop();

    for (length_t i = 1; i <= full_block_count; i++)
    {
        auto & full_buff = logic_blocks->access(_inode.block_filed[starting_block + i]);
        read_len += full_buff.read(output_buffer + read_len, BLOCK_SIZE, 0);
        full_buff.drop();
    }

    if (last_block_read_len)
    {
        auto &last_buff = logic_blocks->access(_inode.block_filed[starting_block + full_block_count + 1]);
        read_len += last_buff.read(output_buffer + read_len, last_block_read_len, 0);
        last_buff.drop();
    }

    return read_len;
}

length_t inode::read_in_2nd_level(char * output_buffer, length_t length, length_t offset)
{
    return 0;
}

length_t inode::read_in_3rd_level(char * output_buffer, length_t length, length_t offset)
{
    return 0;
}

length_t inode::write_in_1st_level(const char * input_buffer, length_t length, length_t offset)
{
    length_t write_len;
    length_t starting_block = offset / BLOCK_SIZE;
    length_t starting_block_off = offset % BLOCK_SIZE;
    length_t starting_block_write_len = MIN(BLOCK_SIZE - starting_block_off, length);
    length_t remaining_size = length - starting_block_write_len;
    length_t full_block_count = remaining_size / BLOCK_SIZE;
    length_t last_block_write_len = remaining_size > BLOCK_SIZE ? remaining_size % BLOCK_SIZE : 0;
    auto _inode = get_inode();

    auto & starting_buff = logic_blocks->access(_inode.block_filed[starting_block]);
    write_len = starting_buff.write(input_buffer, starting_block_write_len, starting_block_off);
    starting_buff.drop();

    for (length_t i = 1; i <= full_block_count; i++)
    {
        auto & full_buff = logic_blocks->access(_inode.block_filed[starting_block + i]);
        write_len += full_buff.write(input_buffer + write_len, BLOCK_SIZE, 0);
        full_buff.drop();
    }

    if (last_block_write_len)
    {
        auto &last_buff = logic_blocks->access(_inode.block_filed[starting_block + full_block_count + 1]);
        write_len += last_buff.write(input_buffer + write_len, last_block_write_len, 0);
        last_buff.drop();
    }

    return write_len;
}

length_t inode::write_in_2nd_level(const char * output_buffer, length_t length, length_t offset)
{
    return 0;
}

length_t inode::write_in_3rd_level(const char * output_buffer, length_t length, length_t offset)
{
    return 0;
}

opera_inode_t inode::get_inode()
{
    opera_inode_t _inode;
    auto & buff = logic_blocks->access(inode_logic_block_number);
    buff.read((char*)&_inode, sizeof (_inode), 0);
    buff.drop();

    return _inode;
}

void inode::save_inode(opera_inode_t _inode)
{
    auto & buff = logic_blocks->access(inode_logic_block_number);
    buff.write((char*)&_inode, sizeof (_inode), 0);
    buff.drop();
}

void inode::add_blocks_in_1st_level(length_t block_count, length_t inode_filed_end_off)
{
    opera_inode_t _inode = get_inode();
    for (uint64_t i = 0; i < block_count; i++)
    {
        _inode.block_filed[inode_filed_end_off + i] = logic_blocks->get_free_logic_block();
    }
    save_inode(_inode);
}

void inode::add_blocks_in_2nd_level(length_t block_count, length_t inode_filed_end_off)
{

}

void inode::add_blocks_in_3rd_level(length_t block_count, length_t inode_filed_end_off)
{

}

void inode::del_blocks_in_1st_level(length_t block_count, length_t inode_filed_end_off)
{
    opera_inode_t _inode = get_inode();
    for (uint64_t i = 0; i < block_count; i++)
    {
        logic_blocks->delete_block(_inode.block_filed[inode_filed_end_off + i]);
        _inode.block_filed[inode_filed_end_off + i] = 0;
    }
    save_inode(_inode);
}

void inode::del_blocks_in_2nd_level(length_t block_count, length_t inode_filed_end_off)
{

}

void inode::del_blocks_in_3rd_level(length_t block_count, length_t inode_filed_end_off)
{

}

length_t inode::access_block_in_2nd_level(length_t)
{
    return 0;
}

length_t inode::access_block_in_3rd_level(length_t)
{
    return 0;
}

void inode::i_truncate(length_t new_length)
{
    auto _inode = get_inode();

    if (_inode.file_len == new_length)
    {
        return;
    }

    auto block_count = (_inode.file_len / BLOCK_SIZE) + ((_inode.file_len % BLOCK_SIZE == 0) ? 0 : 1);
    auto new_block_count = (new_length / BLOCK_SIZE) + ((new_length % BLOCK_SIZE == 0) ? 0 : 1);

    if (_inode.file_len == 0)
    {
        block_count = 0;
    }

    if (new_length == 0)
    {
        new_block_count = 0;
    }

    _inode.file_len = new_length;
    save_inode(_inode);

    if (block_count == new_block_count)
    {
        return;
    }

    if (block_count < new_block_count)
    {
        add_blocks(new_block_count - block_count, block_count);
    }
    else
    {
        del_blocks(block_count - new_block_count, block_count);
    }
}

void inode::write_block_number_in_haccess_block(length_t haccess_block_num,
                                                length_t offset,
                                                length_t val)
{
    auto & buff = logic_blocks->access(haccess_block_num);
    buff.write((char*)&val, sizeof (length_t), offset * sizeof (length_t));
    buff.drop();
}

length_t inode::read_block_number_in_haccess_block(length_t haccess_block_num,
                                                   length_t offset)
{
    length_t ret;
    auto & buff = logic_blocks->access(haccess_block_num);
    buff.write((char*)&ret, sizeof (length_t), offset * sizeof (length_t));
    buff.drop();
    return ret;
}

void inode::add_blocks(length_t block_count, length_t block_offset)
{
    length_t end_point = block_offset + block_count;

    // starts in first level, ends in first level
    if (block_offset < FIRST_LEVEL_BLOCK_COUNT
     && end_point <= FIRST_LEVEL_BLOCK_COUNT)
    {
        add_blocks_in_1st_level(block_count, block_offset);
        return;
    }

    // starts in first level, ends in second level
    if (block_offset < FIRST_LEVEL_BLOCK_COUNT
        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT))
    {
        add_blocks_in_1st_level(FIRST_LEVEL_BLOCK_COUNT - block_offset, block_offset);
        add_blocks_in_2nd_level(end_point - FIRST_LEVEL_BLOCK_COUNT, 0);
        return;
    }

    // starts in first level, ends in third level
    if (block_offset < FIRST_LEVEL_BLOCK_COUNT)
//        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT + THIRD_LEVEL_BLOCK_COUNT))
    {
        add_blocks_in_1st_level(FIRST_LEVEL_BLOCK_COUNT - block_offset, block_offset);
        add_blocks_in_2nd_level(SECOND_LEVEL_BLOCK_COUNT, 0);
        add_blocks_in_3rd_level(end_point - SECOND_LEVEL_BLOCK_COUNT, 0);
        return;
    }

    // starts in second level, ends in second level
    if (block_offset > FIRST_LEVEL_BLOCK_COUNT
        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT))
    {
        add_blocks_in_2nd_level(block_count, block_offset - FIRST_LEVEL_BLOCK_COUNT);
        return;
    }

    // starts in second level, ends in third level
    if (block_offset > FIRST_LEVEL_BLOCK_COUNT)
//        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT + THIRD_LEVEL_BLOCK_COUNT))
    {
        add_blocks_in_2nd_level(SECOND_LEVEL_BLOCK_COUNT - (block_offset - FIRST_LEVEL_BLOCK_COUNT),
                                block_offset - FIRST_LEVEL_BLOCK_COUNT);
        add_blocks_in_3rd_level(end_point - SECOND_LEVEL_BLOCK_COUNT, 0);
        return;
    }

    // starts in third level, ends in third level
    if (block_offset > SECOND_LEVEL_BLOCK_COUNT)
//        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT + THIRD_LEVEL_BLOCK_COUNT))
    {
        add_blocks_in_3rd_level(end_point - SECOND_LEVEL_BLOCK_COUNT,
                                block_offset - SECOND_LEVEL_BLOCK_COUNT);
        return;
    }
}

void inode::del_blocks(length_t block_count, length_t block_offset)
{
    length_t end_point = block_offset + block_count;

    // starts in first level, ends in first level
    if (block_offset < FIRST_LEVEL_BLOCK_COUNT
        && end_point <= FIRST_LEVEL_BLOCK_COUNT)
    {
        del_blocks_in_1st_level(block_count, block_offset);
        return;
    }

    // starts in first level, ends in second level
    if (block_offset < FIRST_LEVEL_BLOCK_COUNT
        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT))
    {
        del_blocks_in_1st_level(FIRST_LEVEL_BLOCK_COUNT - block_offset, block_offset);
        del_blocks_in_2nd_level(end_point - FIRST_LEVEL_BLOCK_COUNT, 0);
        return;
    }

    // starts in first level, ends in third level
    if (block_offset < FIRST_LEVEL_BLOCK_COUNT)
//        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT + THIRD_LEVEL_BLOCK_COUNT))
    {
        del_blocks_in_1st_level(FIRST_LEVEL_BLOCK_COUNT - block_offset, block_offset);
        del_blocks_in_2nd_level(SECOND_LEVEL_BLOCK_COUNT, 0);
        del_blocks_in_3rd_level(end_point - SECOND_LEVEL_BLOCK_COUNT, 0);
        return;
    }

    // starts in second level, ends in second level
    if (block_offset > FIRST_LEVEL_BLOCK_COUNT
        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT))
    {
        del_blocks_in_2nd_level(block_count, block_offset - FIRST_LEVEL_BLOCK_COUNT);
        return;
    }

    // starts in second level, ends in third level
    if (block_offset > FIRST_LEVEL_BLOCK_COUNT)
//        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT + THIRD_LEVEL_BLOCK_COUNT))
    {
        del_blocks_in_2nd_level(SECOND_LEVEL_BLOCK_COUNT - (block_offset - FIRST_LEVEL_BLOCK_COUNT),
                                block_offset - FIRST_LEVEL_BLOCK_COUNT);
        del_blocks_in_3rd_level(end_point - SECOND_LEVEL_BLOCK_COUNT, 0);
        return;
    }

    // starts in third level, ends in third level
    if (block_offset > SECOND_LEVEL_BLOCK_COUNT)
//        && end_point <= (FIRST_LEVEL_BLOCK_COUNT + SECOND_LEVEL_BLOCK_COUNT + THIRD_LEVEL_BLOCK_COUNT))
    {
        del_blocks_in_3rd_level(end_point - SECOND_LEVEL_BLOCK_COUNT,
                                block_offset - SECOND_LEVEL_BLOCK_COUNT);
        return;
    }
}

length_t inode::get_file_len()
{
    opera_inode_t _inode = get_inode();
    return _inode.file_len;
}
