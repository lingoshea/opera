#include <fuse_related/operations.h>
#include <fuse_related/inode.h>
#include <sys_related/opera_error.h>
#include <fuse_related/fuse_ops.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>

#define PERM_READ 0b100
#define PERM_WRTE 0b010
#define PERM_EXEC 0b001


bool check_perm(mode_t mode, opera_inode_t & inode)
{
    auto context = fuse_get_context();
    uint16_t pair = mode;

    // root can just ignore the permission settings
    if (context->gid == 0 && context->uid == 0)
    {
        return true;
    }

    if (context->gid == inode.gid)
    {
        // myself
        if (context->uid == inode.uid)
        {
            pair <<= 6;
        }
        else // same group
        {
            pair <<= 3;
        }

        return inode.mode & pair;
    }

    return inode.mode & pair;
}


int opera_getattr(const char *path, struct stat *stbuf)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        auto inode_info = _inode.get_inode();

        stbuf->st_nlink = inode_info.links;
        stbuf->st_size  = inode_info.file_len;
        stbuf->st_gid   = inode_info.gid;
        stbuf->st_uid   = inode_info.uid;
        stbuf->st_mode  = inode_info.mode;
        stbuf->st_blocks = (inode_info.file_len / BLOCK_SIZE) +
                           (inode_info.file_len % BLOCK_SIZE == 0 ? 0 : 1);

        stbuf->st_atim  = inode_info.access_time;
        stbuf->st_ctim  = inode_info.change_time;
        stbuf->st_mtim  = inode_info.modify_time;

        inode_info.access_time = current_time();
        _inode.save_inode(inode_info);

        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }

}

int opera_open(const char *path, struct fuse_file_info *fi)
{
    return 0;
}

int opera_release(const char *, struct fuse_file_info *)
{
    try
    {
        opera::filesystem_device.sync();
        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int opera_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                 off_t offset, struct fuse_file_info *fi)
{
    try
    {
        path_t vpath(path);
        auto inode_num = opera::namei(vpath);
        inode my_inode;
        my_inode.init(&opera::filesystem_logic_blocks, inode_num);
        auto dentry_count = my_inode.get_file_len() / sizeof (opera_dentry_t);

        for (uint64_t i = 0; i < dentry_count; i++)
        {
            opera_dentry_t dentry { };
            if (my_inode.read((char*)&dentry, sizeof (dentry), sizeof (dentry) * i)
                != sizeof (dentry))
            {
                break;
            }

            if (dentry.inode_logic_block_num != 0)
            {
                filler(buf, dentry.filename, nullptr, 0);
            }
        }

        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_fsync (const char *, int, struct fuse_file_info *)
{
    opera::filesystem_device.sync();
    return 0;
}

int opera_read(const char *path, char *buf, size_t size,
              off_t offset, struct fuse_file_info *fi)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        return _inode.read(buf, size, offset);

    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_write(const char *path, const char *buf, size_t size,
               off_t offset, struct fuse_file_info *fi)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        return _inode.write(buf, size, offset);

    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    try
    {
        auto context = fuse_get_context();
        path_t vpath(path);
        opera::mknod(vpath,
                     mode,
                     context->gid,
                     context->uid);
        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_unlink(const char *path)
{
    try
    {
        opera::unlink(path_t(path));
        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}


int opera_mknod(const char *path, mode_t mode, dev_t dev)
{
    try
    {
        auto context = fuse_get_context();
        path_t vpath(path);
        opera::mknod(vpath, mode, context->gid, context->uid, dev);

        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_mkdir(const char *path, mode_t mode)
{
    try
    {
        auto context = fuse_get_context();
        path_t vpath(path);
        opera::mknod(vpath, mode, context->gid, context->uid);

        return 0;
    }
    catch (opera_error_t &error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_chmod(const char *path, mode_t mode)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        auto _inode_info = _inode.get_inode();
        _inode_info.mode = mode;
        _inode.save_inode(_inode_info);

        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_chown(const char *path, uid_t uid, gid_t gid)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        auto _inode_info = _inode.get_inode();
        _inode_info.uid = uid;
        _inode_info.gid = gid;
        _inode.save_inode(_inode_info);

        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_truncate(const char *path, off_t size)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        _inode.i_truncate(size);

        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_utimens(const char *path, const struct timespec tv[2])
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        auto _inode_info = _inode.get_inode();
        _inode_info.access_time = tv[0];
        _inode_info.modify_time = tv[1];
        _inode.save_inode(_inode_info);

        return 0;
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -1;
    }
}

int opera_access (const char * path, int mode)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = opera::namei(vpath);
        inode _inode;
        _inode.init(&opera::filesystem_logic_blocks, inode_block_num);
        auto _inode_info = _inode.get_inode();

        switch (mode)
        {
            case F_OK:
                // inode exists
                return 0;

            case R_OK:
                if (check_perm(PERM_READ, _inode_info))
                {
                    return 0;
                }

                return -EACCES;

            case W_OK:
                if (check_perm(PERM_WRTE, _inode_info))
                {
                    return 0;
                }

                return -EACCES;

            case X_OK:
                if (check_perm(PERM_EXEC, _inode_info))
                {
                    return 0;
                }

                return -EACCES;

            default:
                return -EACCES;
        }
    }
    catch (opera_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -error.get_errno();
    }
    catch (...)
    {
        return -EPERM;
    }
}


