#include <stdafx.h>
#include <sys_related/opera_error.h>
#include <cerrno>
#include <sstream>
#include <cstring>
#include <execinfo.h>

const char * opera_error_t::what() const noexcept
{
    return error.c_str();
}

opera_error_t::opera_error_t(
                           const std::string& str,
                           const std::string& filename,
                           const std::string& function,
                           unsigned long long line,
                           void * parent,
                           uint32_t additional_error_code) noexcept
{
    std::stringstream sserror;
    void *array [OPERA_ERROR_MAX_BACKTRACE_SIZE];
    char **strings;
    int size, i;

    _state_snapshot._errno = errno;
    _state_snapshot.parent = parent;
    _state_snapshot.filename = filename;
    _state_snapshot.function = function;
    _state_snapshot.line = line;
    _state_snapshot.additional_error_code = additional_error_code;

    size = backtrace (array, OPERA_ERROR_MAX_BACKTRACE_SIZE);
    strings = backtrace_symbols (array, size);
    if (strings != nullptr)
    {
        sserror << "\n<== !! Exception occurred !! ==>\nObtained stack frame(s):\n";

        for (i = 0; i < size; i++)
        {
            sserror << strings[i] << "\n";
        }
    }

    free (strings);

    sserror << "From " << _state_snapshot.filename
            << ":" << _state_snapshot.line
            << ":" << _state_snapshot.function
            << ": " << str << " (errno=\"" << strerror(errno) << "\")\n"
            << "<== End of exception log ==>\n";

    error = sserror.str();
}

int opera_error_t::get_errno() const
{
    return _state_snapshot._errno;
}

uint32_t opera_error_t::get_xerrcode() const
{
    return _state_snapshot.additional_error_code;
}

const char * ecodetostr(uint64_t ecode)
{
    switch (ecode)
    {
        default:
        case ERROR_DULL_DESCRIPTION:
            return "Dull description";

        case ERROR_BUFFER_DEPLETED:
            return "Buffer depleted";

        case ERROR_SYS_IO_FAILED:
            return "System level I/O failed";

        case ERROR_READONLY_BUFFER:
            return "Readonly buffer";

        case ERROR_LOGIC_BLOCK_DEPLETED:
            return "Logic block depleted";

        case ERROR_ENTRY_NOT_FOUND:
            return "Entry not found";
    }
}
