#include <sys_related/opera_time.h>
#include <unistd.h>

void opera_usleep(length_t duration)
{
    usleep(duration);
}

