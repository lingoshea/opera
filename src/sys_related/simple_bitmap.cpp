#include <stdafx.h>
#include <sys_related/simple_bitmap.h>
#include <sys_related/opera_error.h>
#include <cstring>

void simple_bitmap::init(length_t _bitmap_length)
{
    bitmap_length = _bitmap_length;
    length_t bit_count = LENGTH_BY_TYPE(_bitmap_length, bit_t);
    bitmap = new bit_t [bit_count];
    memset(bitmap, 0, bit_count);
}

void simple_bitmap::cleanup()
{
    delete[] bitmap;
    bitmap = nullptr;
    bitmap_length = 0;
}

length_t simple_bitmap::get_free_node()
{
    if (last_allocated_node + 1 == bitmap_length)
    {
        last_allocated_node = 0;
    }

    for (length_t i = last_allocated_node; i < bitmap_length; i++)
    {
        if (!get_status_of(i))
        {
            last_allocated_node = i;
            set_status_of(i, true);
            return i;
        }
    }

    for (length_t i = 0; i < bitmap_length; i++)
    {
        if (!get_status_of(i))
        {
            last_allocated_node = i;
            set_status_of(i, true);
            return i;
        }
    }

    code_throw_hmnx_error(ERROR_BUFFER_DEPLETED);
}

bool simple_bitmap::get_status_of(length_t bitoff)
{
    if (bitoff >= bitmap_length)
    {
        return false;
    }

    length_t bit_ops = bitoff / __BIT_T_BIT_COUNT__;
    length_t bit_ioff = bitoff % __BIT_T_BIT_COUNT__;
    bit_t comp = 0x01 << bit_ioff;

    return bitmap[bit_ops] & comp;
}

void simple_bitmap::set_status_of(length_t bitoff, bool status)
{
    if (bitoff >= bitmap_length)
    {
        return;
    }

    length_t bit_ops = bitoff / __BIT_T_BIT_COUNT__;
    length_t bit_ioff = bitoff % __BIT_T_BIT_COUNT__;
    bit_t comp = 0x01 << bit_ioff;
    comp = ~comp;
    bitmap[bit_ops] &= comp; // clear bit

    if (status)
    {
        // set bit
        comp = ~comp;
        bitmap[bit_ops] |= comp;
    }
}
