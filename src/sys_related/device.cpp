#include <sys_related/device.h>
#include <sys_related/opera_error.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>

void device::init(const char * path_to_device, length_t buffer_len)
{
    file_des = open(path_to_device, O_RDWR);

    if (file_des == -1)
    {
        str_throw_hmnx_error("Cannot open device");
    }

    if (lseek(file_des, 0, SEEK_END) == -1)
    {
        str_throw_hmnx_error("Cannot seek device");
    }

    file_size = lseek(file_des, 0, SEEK_CUR);

    if (file_size == 0)
    {
        str_throw_hmnx_error("Cannot tell device size");
    }

    buffer_bitmap.init(buffer_len);
    block_buffer = new buffer[buffer_len];
}

buffer &device::alloc_bbuf(length_t block_number)
{
    auto it = access_map.find(block_number);
    if (it != access_map.end())
    {
        it->second->dropped = 0;
        return *it->second;
    }

    try_again:
    try
    {
        auto free_node = buffer_bitmap.get_free_node();
        buffer &bbuffer = block_buffer[free_node];
        bbuffer.refreshed = 0;
        bbuffer.file_desc = file_des;
        bbuffer.on_disk_block_number = block_number;
        bbuffer.dropped = 0;
        bbuffer.dirty = 0;

        access_map.emplace(block_number, &bbuffer);
        block_to_id_list.emplace(block_number, free_node);
        access_list.emplace_back(block_number);

        return bbuffer;
    }
    catch (opera_error_t & error)
    {
        if (error.get_xerrcode() == ERROR_BUFFER_DEPLETED)
        {
            sync();
            goto try_again;
        }

        throw ;
    }
    catch (...)
    {
        throw ;
    }
}

void device::sync()
{
    auto it = access_list.begin();
    while (it != access_list.end())
    {
        auto buff = access_map.at(*it);

        if (buff->dropped)
        {
            buff->write_to_disk();

            buffer_bitmap.set_status_of(block_to_id_list.at(*it), false);
            access_map.erase(*it);
            block_to_id_list.erase(*it);
            access_list.erase(it);
        }
        else
        {
            it++;
        }
    }
}

void device::cleanup()
{
    if (!file_des)
    {
        return;
    }

    // force sync
    auto it = access_list.begin();
    while (it != access_list.end())
    {
        auto buff = access_map.at(*it);
        buff->write_to_disk();
        buffer_bitmap.set_status_of(block_to_id_list.at(*it), false);
        access_map.erase(*it);
        block_to_id_list.erase(*it);
        access_list.erase(it);
    }

    buffer_bitmap.cleanup();
    delete[] block_buffer;
    ::close(file_des);

    file_des = 0;
    block_buffer = nullptr;
}

length_t buffer::read(char * buffer, length_t length, length_t offset)
{
    if (offset > BLOCK_SIZE)
    {
        return 0;
    }

    if (offset + length > BLOCK_SIZE)
    {
        length = BLOCK_SIZE - offset;
    }

    if (!refreshed)
    {
        refresh_from_disk();
    }

    memcpy(buffer, data + offset, length);
    return length;
}

length_t buffer::write(const char * buffer, length_t length, length_t offset)
{
    if (dropped)
    {
        code_throw_hmnx_error(ERROR_READONLY_BUFFER);
    }

    if (offset >= BLOCK_SIZE)
    {
        return 0;
    }

    if (offset + length > BLOCK_SIZE)
    {
        length = BLOCK_SIZE - offset;
    }

    if (!refreshed && length != BLOCK_SIZE)
    {
        refresh_from_disk();
    }

    memcpy(data + offset, buffer, length);

    dirty = 1;
    refreshed = 1; // overwrite

    return length;
}

void buffer::refresh_from_disk()
{
    if (::lseek(file_desc, BLOCK_SIZE * on_disk_block_number, SEEK_SET) == -1)
    {
        code_throw_hmnx_error(ERROR_SYS_IO_FAILED);
    }

    if (::read(file_desc, data, BLOCK_SIZE) != BLOCK_SIZE)
    {
        code_throw_hmnx_error(ERROR_SYS_IO_FAILED);
    }

    refreshed = 1;
}

void buffer::write_to_disk()
{
    if (!dirty)
    {
        return;
    }

    if (::lseek(file_desc, BLOCK_SIZE * on_disk_block_number, SEEK_SET) == -1)
    {
        code_throw_hmnx_error(ERROR_SYS_IO_FAILED);
    }

    if (::write(file_desc, data, BLOCK_SIZE) != BLOCK_SIZE)
    {
        code_throw_hmnx_error(ERROR_SYS_IO_FAILED);
    }
}
