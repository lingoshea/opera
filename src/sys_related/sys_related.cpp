#include <sys_related/sys_related.h>
#include <thread>

unsigned int thread_count()
{
    return std::thread::hardware_concurrency();
}
