#ifndef OPERA_FS_BITMAP_H
#define OPERA_FS_BITMAP_H

#include <stdafx.h>
#include <sys_related/device.h>

// !!! Bitmap counts from 0 while logic block starts count from 1 !!!

class fs_bitmap_t
{
private:
    device * file;
    length_t bitmap_starting_block;
    length_t bitmap_block_count;
    length_t bitmap_size;
    length_t last_allocated_node;

public:
    void init(device*   /* device */,
              length_t  /* bitmap starting block */,
              length_t  /* bitmap block count */,
              length_t  /* bitmap count */,
              length_t  /* last allocated node */);

    length_t get_free_node(); // return a free node and set corresponding node as true

    bool get_status(length_t /* bit offset */);
    void set_status(length_t /* bit offset */, bool /* status */);

    length_t get_last_allocated_node() const { return last_allocated_node; }
};


#endif //OPERA_FS_BITMAP_H
