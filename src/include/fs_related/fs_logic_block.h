#ifndef OPERA_FS_LOGIC_BLOCK_H
#define OPERA_FS_LOGIC_BLOCK_H

#include <fs_related/fs_bitmap.h>
#include <sys_related/device.h>

class logic_block
{
private:
    device *    file;
    length_t    logic_block_starting_block;
    length_t    logic_block_count;

public:
    fs_bitmap_t bitmap;

    void init(device * /* device */,
            length_t /* bitmap starting block */,
            length_t /* bitmap block count */,
            length_t /* last allocated node */,
            length_t /* logic block starting block */,
            length_t /* logic block count */);

    buffer & access(length_t /* logic block number (starts from 1) */);

    length_t get_free_logic_block();  // get a free block number (starts count from 1)
                                // and set corresponding node as used

    void delete_block(length_t /* logic block number (starts from 1) */); // free a logic block

    // return last allocated block number (starts from 0)
    length_t get_last_allocated_block_number() const { return bitmap.get_last_allocated_node(); };
};

#endif //OPERA_FS_LOGIC_BLOCK_H
