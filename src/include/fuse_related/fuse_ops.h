#ifndef OPERA_FUSE_OPS_H
#define OPERA_FUSE_OPS_H

#define FUSE_USE_VERSION 31
#include <fuse.h>

int opera_getattr   (const char *, struct stat *);
int opera_open      (const char *, struct fuse_file_info *);
int opera_release   (const char *, struct fuse_file_info *);
int opera_readdir   (const char *, void *, fuse_fill_dir_t, off_t, struct fuse_file_info *);
int opera_fsync     (const char *, int, struct fuse_file_info *);
int opera_read      (const char *, char *, size_t, off_t, struct fuse_file_info *);
int opera_write     (const char *, const char *, size_t, off_t, struct fuse_file_info *);
int opera_create    (const char *, mode_t, struct fuse_file_info *);
int opera_unlink    (const char *);
int opera_mknod     (const char *, mode_t, dev_t);
int opera_mkdir     (const char *, mode_t);
int opera_chmod     (const char *, mode_t);
int opera_chown     (const char *, uid_t, gid_t);
int opera_truncate  (const char *, off_t);
int opera_utimens   (const char *, const struct timespec [2]);
int opera_access    (const char *, int);

#endif //OPERA_FUSE_OPS_H
