#ifndef OPERA_INODE_H
#define OPERA_INODE_H

#include <fs_related/fs_logic_block.h>
#include <opera.h>

class inode
{
private:
    logic_block * logic_blocks;
    length_t inode_logic_block_number;

    length_t read_in_1st_level(char * /* output buffer */,
                               length_t /* read length */,
                               length_t /* read offset */);

    length_t read_in_2nd_level(char * /* output buffer */,
                               length_t /* read length */,
                               length_t /* read offset */);

    length_t read_in_3rd_level(char * /* output buffer */,
                               length_t /* read length */,
                               length_t /* read offset */);

    length_t write_in_1st_level(const char * /* input buffer */,
                               length_t /* write length */,
                               length_t /* write offset */);

    length_t write_in_2nd_level(const char * /* input buffer */,
                               length_t /* write length */,
                               length_t /* write offset */);

    length_t write_in_3rd_level(const char * /* input buffer */,
                               length_t /* write length */,
                               length_t /* write offset */);

    void add_blocks(length_t /* block count */, length_t /* block offset */); // add blocks in specific location
    void del_blocks(length_t /* block count */, length_t /* block offset */); // add blocks in specific location

    void add_blocks_in_1st_level(length_t /* block count */, length_t /* block offset */);
    void add_blocks_in_2nd_level(length_t /* block count */, length_t /* block offset */);
    void add_blocks_in_3rd_level(length_t /* block count */, length_t /* block offset */);

    void del_blocks_in_1st_level(length_t /* block count */, length_t /* block offset */);
    void del_blocks_in_2nd_level(length_t /* block count */, length_t /* block offset */);
    void del_blocks_in_3rd_level(length_t /* block count */, length_t /* block offset */);

    length_t access_block_in_2nd_level(length_t /* offset */); // access block in 2nd block filed
    length_t access_block_in_3rd_level(length_t /* offset */); // access block in 3rd block filed

    void write_block_number_in_haccess_block
                                          (length_t /* hacess block number */,
                                           length_t /* in block offset (in 8 bytes)*/,
                                           length_t /* block number that wish to be written */);

    length_t read_block_number_in_haccess_block
                                            (length_t /* hacess block number */,
                                             length_t /* in block offset (in 8 bytes)*/);

public:

    void init(logic_block * /* logic blocks */,
          length_t /* inode logic block number */);

    length_t read(char * /* output buffer */,
                  length_t /* read length */,
                  length_t /* read offset */);

    length_t write(const char * /* output buffer */,
                  length_t /* write length */,
                  length_t /* write offset */);

    length_t get_file_len(); // return file length

    opera_inode_t get_inode();      // get inode info
    void save_inode(opera_inode_t); // save inode info

    void i_truncate(length_t /* new size */);
};


#endif //OPERA_INODE_H
