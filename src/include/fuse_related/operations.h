#ifndef OPERA_OPERATIONS_H
#define OPERA_OPERATIONS_H

#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdafx.h>
#include <fuse_related/path_t.h>
#include <fs_related/fs_logic_block.h>
#include <fuse_related/inode.h>

namespace opera
{
    extern device filesystem_device;
    extern logic_block filesystem_logic_blocks;
    length_t namei(path_t /* pathname */); // return inode of given pathname

    void mknod(path_t /* path to node */,
               unsigned int   /* mode */,
               gid_t /* gid */,
               uid_t /* uid */,
               dev_t device = 0); // make node

    void unlink(const path_t& /* path to node */);
    void rmdentry(path_t /* path to dentry */);
    length_t find_entry_in_dir(inode &_inode /* dir inode */, const std::string &entry /* entry name */);
}

#endif //OPERA_OPERATIONS_H
