#ifndef OPERA_PATH_T_H
#define OPERA_PATH_T_H

#include <vector>
#include <string>

typedef std::vector < std::string >::iterator path_iterator_t;

// This class auto translates FUSE path to vector path
class path_t
{
protected:
    std::vector < std::string > _path;

public:
    const std::vector < std::string > & path = _path;
    std::string operator [](unsigned int);

    explicit path_t(std::string fuse_path);

    path_iterator_t begin()     { return _path.begin(); }   // return it begin
    path_iterator_t end()       { return _path.end(); }     // return it end

    // pop back and return last element
    std::string pop()           { std::string str = *(_path.end() - 1); _path.pop_back(); return str; }

    // return path depth
    size_t depth() const         { return _path.size(); }
};


#endif //OPERA_PATH_T_H
