#ifndef OPERA_STDAFX_H
#define OPERA_STDAFX_H

#include <sys_related/types.h>

#define SMALLEST_CACHE_SIZE /* 4Kb */ (4096)
#define CACHE_SIZE /* 4Kb */ SMALLEST_CACHE_SIZE
#define BLOCK_SIZE /* 4Kb */ SMALLEST_CACHE_SIZE

#define OPERA_ERROR_MAX_BACKTRACE_SIZE 64

extern bool debug;

#define THREAD_SLEEP_DURATION 1000

// #define LENGTH_BY_TYPE(length, type) ( ((length) / sizeof(type)) + ((length) % sizeof(type) == 0 ? 0 : 1) )
#define LENGTH_BY_TYPE(length, type) ( ((length) / sizeof(type)) + ((length) % sizeof(type) == 0) )

#define FILESYSTEM_MAGIC_NUMBER 0xFC7478BA5609AD64
#define FILESYSTEM_BLOCK_HASH_SEED FILESYSTEM_MAGIC_NUMBER

struct timespec current_time();

#define ROOT_INODE_LOGIC_BLOCK_RESIDENCE 1

#endif //OPERA_STDAFX_H
