#ifndef OPERA_OPERA_H
#define OPERA_OPERA_H

#include <cstdint>
#include <stdafx.h>
#include <ctime>

struct __attribute__((packed))
opera_super_block_t
{
    uint64_t magic;
    uint64_t bitmap_block_count;
    uint64_t logic_block_count;
    uint64_t free_logic_block_count;
    uint64_t last_allocated_logic_number; // !! starts count from 0 !!
    uint64_t mounted:1;
    struct timespec last_mount_time;
    uint64_t magic_cmp;
};

struct __attribute__((packed))
opera_dentry_t
{
    uint64_t inode_logic_block_num; // 8 byte
    char filename [256 - 8];
};

struct __attribute__((packed))
opera_inode_t
{
    uint32_t links;
    uint32_t mode;
    uint64_t file_len;

    // 32*4096 + 32*(4096/8)*4096 + 64*(4096/8)*(4096/8)*4096 ~= 64 Gb (starts count from 1)
    uint64_t block_filed[32 + 32 + 64];

    gid_t    gid;
    uid_t    uid;
    struct timespec access_time;
    struct timespec change_time;
    struct timespec modify_time;
};

#define FIRST_LEVEL_LENGTH  ((uint64_t)(32 * (uint64_t)BLOCK_SIZE))
#define SECOND_LEVEL_LENGTH ((uint64_t)(32 * (BLOCK_SIZE/8) * (uint64_t)BLOCK_SIZE))
#define THIRD_LEVEL_LENGTH  ((uint64_t)(64 * (BLOCK_SIZE/8) * (BLOCK_SIZE/8) * (uint64_t)BLOCK_SIZE))

#define FIRST_LEVEL_BLOCK_COUNT     (32)
#define SECOND_LEVEL_BLOCK_COUNT    (32 * (BLOCK_SIZE/8))
#define THIRD_LEVEL_BLOCK_COUNT     (64 * (BLOCK_SIZE/8) * (BLOCK_SIZE/8))

#endif //OPERA_OPERA_H
