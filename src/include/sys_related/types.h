#ifndef OPERA_TYPES_H
#define OPERA_TYPES_H

#include <sys/types.h>
#include <cstdint>

#ifndef __FD_T_DEFINED__
# define __FD_T_DEFINED__
typedef int fd_t;
# define __FD_T_SIZE__      (sizeof(fd_t))
#endif // __FD_T_DEFINED__

#ifndef __LENGTH_T_DEFINED__
# define __LENGTH_T_DEFINED__
typedef uint64_t length_t;
# define __LENGTH_T_SIZE__  (sizeof(length_t))
#endif // __LENGTH_T_DEFINED__

#ifndef __OFFSET_T_DEFINED__
# define __OFFSET_T_DEFINED__
typedef int64_t offset_t;
# define __OFFSET_T_SIZE__  (sizeof(offset))
#endif // __OFFSET_T_DEFINED__

#ifndef __BIT_T_DEFINED__
# define __BIT_T_DEFINED__
typedef uint8_t bit_t;
# define __BIT_T_SIZE__     (sizeof(bit_t))
# define __BIT_T_BIT_COUNT__ (8)
#endif // __BIT_T_DEFINED__

#endif // OPERA_TYPES_H
