#ifndef OPERA_SIMPLE_BITMAP_H
#define OPERA_SIMPLE_BITMAP_H

#include <sys_related/types.h>

class simple_bitmap
{
private:
    bit_t * bitmap = nullptr;   /* bits
                                 * allocated by [void init(length_t)]
                                 * deallocated by [cleanup()] */

    length_t bitmap_length = 0; /* length of bits
                                 * inited by [void init(length_t)] */

    length_t last_allocated_node = 0;

public:
    bool get_status_of(length_t /* bit offset */); // get the specific status of one bit in bitmap
    void set_status_of(length_t /* bit offset */, bool /* status */); // set the specific status of one bit in bitmap

    simple_bitmap() = default;

    void init(length_t /* bitmap length */);    // init bitmap
    void cleanup(); // deallocate bitmap

    length_t get_free_node(); // allocate a free node and mark the corresponding node as 1
                          // if depleted, BUFFER_DEPLETED would be thrown

    ~simple_bitmap() { cleanup(); }
};

#endif //OPERA_SIMPLE_BITMAP_H
