#ifndef OPERA_DEVICE_H
#define OPERA_DEVICE_H

#include <map>
#include <vector>
#include <stdafx.h>
#include <sys_related/simple_bitmap.h>
#include <sys/stat.h>

class device;

class buffer
{
private:
    char data [BLOCK_SIZE];
    uint64_t refreshed:1,
             dirty:1,
             dropped:1;

    length_t on_disk_block_number = 0; // On disk block number, starting from 0
    fd_t     file_desc = 0;

    buffer() = default;

    void refresh_from_disk();
    void write_to_disk();

public:

    length_t read(char * /* output buffer */,
                  length_t /* read length */,
                  length_t /* read offset */);

    length_t write(const char * /* input buffer */,
                  length_t /* write length */,
                  length_t /* write offset */);

    void drop() { dropped = 1; }

    friend class device;
};

class device
{
    fd_t            file_des = 0;
    buffer *        block_buffer = nullptr;
    simple_bitmap   buffer_bitmap;
    std::map < uint64_t /* block number */, buffer * > access_map;
    std::map < uint64_t /* block number */, uint64_t /* buffer ID */ > block_to_id_list;
    std::vector < uint64_t /* block number */ > access_list;
    length_t        file_size;

public:
    device() = default;

    void init(const char *  /* path to file */,
              length_t      /* cache count */);

    buffer & alloc_bbuf(length_t /* block number */); // allocate block buffer

    void sync();    // sync device

    void cleanup(); // cleanup buffer and close device

    ~device() { cleanup(); }

    length_t block_count() const { return file_size / BLOCK_SIZE; }
};


#endif //OPERA_DEVICE_H
