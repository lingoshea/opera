#ifndef OPERA_OPERA_TIME_H
#define OPERA_OPERA_TIME_H

#include <sys_related/types.h>

void opera_usleep(length_t /* sleep duration */); // sleep

#endif //OPERA_OPERA_TIME_H
