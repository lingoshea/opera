#ifndef OPERA_OPERA_ERROR_H
#define OPERA_OPERA_ERROR_H

#include <exception>
#include <string>

#define ERROR_DULL_DESCRIPTION                          0   /* no additional description */
#define ERROR_BUFFER_DEPLETED                           1   /* buffer depleted */
#define ERROR_SYS_IO_FAILED                             2   /* System level I/O failed */
#define ERROR_READONLY_BUFFER                           3   /* Readonly buffer */
#define ERROR_LOGIC_BLOCK_DEPLETED                      4   /* Logic block depleted */
#define ERROR_ENTRY_NOT_FOUND                           5   /* Entry not found */

#define ERROR_NO_IMPLEMENTATION                         255 /* Function not implemented */

class opera_error_t : public std::exception
{
private:
    struct // save the machine state when the error occur
    {
        int _errno = 0;
        std::string filename;
        std::string function;
        unsigned long long line = 0;
        void * parent = nullptr;
        uint32_t additional_error_code;
    } _state_snapshot;

    std::string error;

public:
    explicit opera_error_t(
            const std::string& /* Error description */,
            const std::string& /* Error source filename (Automatically filled by marco) */,
            const std::string& /* Error source function name (Automatically filled by marco) */,
            unsigned long long /* Error source line number (Automatically filled by marco) */,
            void *             /* Error source stack frame starting address (Automatically filled by marco) */,
            uint32_t           /* Internal error code (set to DULL_DESCRIPTION if there's none)*/)
    noexcept;
    const char * what() const noexcept override;


    int get_errno() const;
    uint32_t get_xerrcode() const;
};

const char * ecodetostr(uint64_t /* error code */);

#define str_throw_hmnx_error(str) throw opera_error_t("[ERROR]: " + std::string(str), __FILE__, __FUNCTION__, __LINE__, \
    __builtin_return_address(0), ERROR_DULL_DESCRIPTION)

#define code_throw_hmnx_error(code) throw opera_error_t("[ERROR]: Internal interrupt (" \
    + std::to_string(code) + "): \"" + ecodetostr(code) + "\"", \
    __FILE__, __FUNCTION__, __LINE__, \
    __builtin_return_address(0), (code))

#endif //OPERA_OPERA_ERROR_H
