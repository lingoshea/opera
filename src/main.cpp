#define FUSE_USE_VERSION 31
#include <iostream>
#include <fuse.h>
#include <cstring>
#include <stdafx.h>
#include <opera.h>
#include <fuse_related/operations.h>
#include <sys_related/opera_error.h>
#include <fuse_related/fuse_ops.h>


static struct fuse_operations opera_ops =
        {
                .getattr    = opera_getattr,
//                .readlink   = opera_readlink,
                .mknod      = opera_mknod,
                .mkdir      = opera_mkdir,
                .unlink     = opera_unlink,
                .rmdir      = opera_unlink,
//                .symlink    = opera_symlink,
//                .rename     = opera_rename,
//                .link       = opera_link,
                .chmod      = opera_chmod,
                .chown      = opera_chown,
                .truncate   = opera_truncate,
                .open       = opera_open,
                .read       = opera_read,
                .write      = opera_write,
//                .statfs     = opera_statfs,
//                .flush      = opera_flush,
                .release    = opera_release,
                .fsync      = opera_fsync,
//                .setxattr   = opera_setxattr,
//                .getxattr   = opera_getxattr,
//                .listxattr  = opera_listxattr,
//                .removexattr= opera_removexattr,
                .opendir    = opera_open,
                .readdir    = opera_readdir,
                .releasedir = opera_release,
//                .fsyncdir   = opera_fsyncdir,
//                .destroy    = opera_destroy,
                .access     = opera_access,
                .create     = opera_create,
//                .lock       = opera_lock,
                .utimens    = opera_utimens,
//                .ioctl      = opera_ioctl,
//                .fallocate  = opera_fallocate
        };

static void usage(const char *progname)
{
    printf(
            "usage: %s device mountpoint [options]\n"
            "\n"
            "general options:\n"
            "    -o opt,[opt...]        Mount options.\n"
            "    -h, --help             Print help.\n"
            "    -V, --version          Print version.\n"
            "    -m, --buffer-mem       Specify buffer space memory size (in %dKb).\n"
            "\n", progname, BLOCK_SIZE / 1024);
}

#define PACKAGE_NAME    "FUSE-based Opera Filesystem"
#define PACKAGE_VERSION GENERAL_VERSION

enum {
    KEY_VERSION,
    KEY_HELP,
    KEY_POOL_SZ,
};

static struct fuse_opt hmnx_opts[] = {
        FUSE_OPT_KEY("-V",              KEY_VERSION),
        FUSE_OPT_KEY("--version",       KEY_VERSION),
        FUSE_OPT_KEY("-h",              KEY_HELP),
        FUSE_OPT_KEY("--help",          KEY_HELP),
        FUSE_OPT_KEY("-m ",             KEY_POOL_SZ),
        FUSE_OPT_KEY("--buffer-mem ",   KEY_POOL_SZ),
        FUSE_OPT_END,
};

const char * device_path = nullptr;
static struct fuse_operations ss_nullptr = { };
uint64_t buffer_pool_size = 40960;

static int main_opt_proc(void *, const char *arg, int key,
                         struct fuse_args *outargs)
{
    switch (key)
    {
        case FUSE_OPT_KEY_NONOPT:
            if (device_path == nullptr)
            {
                device_path = strdup(arg);
                return 0;
            }

            return 1;

        case KEY_VERSION:
            printf("%s version: %s\n", PACKAGE_NAME, PACKAGE_VERSION);
            fuse_opt_add_arg(outargs, "--version");
            fuse_main(outargs->argc, outargs->argv, &ss_nullptr, nullptr);
            fuse_opt_free_args(outargs);
            exit(EXIT_SUCCESS);

        case KEY_HELP:
            usage(outargs->argv[0]);
            fuse_opt_add_arg(outargs, "-ho");
            fuse_main(outargs->argc, outargs->argv, &ss_nullptr, nullptr);
            fuse_opt_free_args(outargs);
            exit(EXIT_SUCCESS);

        case KEY_POOL_SZ:
        {
            size_t len = strlen(arg);
            size_t i = 0;

            for (; i < len; i++)
            {
                if (isdigit(arg[i]))
                {
                    break;
                }
            }

            buffer_pool_size = strtol(arg + i, nullptr, 10);
            return 0;
        }

        default:
            return 1;
    }
}

int main(int argc, char **argv)
{
    try
    {
        struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

        if (fuse_opt_parse(&args, nullptr, hmnx_opts, main_opt_proc) == -1)
        {
            str_throw_hmnx_error("Couldn't parse the argument");
        }

        if (!device_path)
        {
            str_throw_hmnx_error("No device path provided!");
        }

        opera_super_block_t _super_block{};

        opera::filesystem_device.init(device_path, buffer_pool_size);

        auto & super_block_buff = opera::filesystem_device.alloc_bbuf(0);
        super_block_buff.read((char*)&_super_block, sizeof (_super_block), 0);

        if (_super_block.magic != FILESYSTEM_MAGIC_NUMBER
         || _super_block.magic_cmp != ~FILESYSTEM_MAGIC_NUMBER)
        {
            str_throw_hmnx_error("Unknown filesystem");
        }

        _super_block.mounted = 1;
        _super_block.last_mount_time = current_time();
        super_block_buff.write((char*)&_super_block, sizeof (_super_block), 0);
        super_block_buff.drop();

        opera::filesystem_logic_blocks.init(&opera::filesystem_device,
                                            1,
                                            _super_block.bitmap_block_count,
                                            _super_block.last_allocated_logic_number - 1,
                                            _super_block.bitmap_block_count + 1,
                                            _super_block.logic_block_count);

        /*
         * s: run single threaded
         * d: enable debugging
         * f: stay in foreground
         */
        fuse_opt_add_arg(&args, "-s");
        fuse_opt_add_arg(&args, "-d");
        fuse_opt_add_arg(&args, "-f");

        // update super block
        int ret = fuse_main(args.argc, args.argv, &opera_ops, nullptr);

        if (ret != 0)
        {
            str_throw_hmnx_error("Cannot mount due to error in FUSE!");
        }

        fuse_opt_free_args(&args);

        std::cout << "[INFO]: Syncing ... do not disconnect your device!" << std::endl;

        auto & super_block_buff_2 = opera::filesystem_device.alloc_bbuf(0);
        _super_block.mounted = 0;
        super_block_buff_2.write((char*)&_super_block, sizeof (_super_block), 0);
        super_block_buff_2.drop();

        opera::filesystem_device.sync();

        // close the device
        opera::filesystem_device.cleanup();

        std::cout << "[INFO]: Filesystem sync finished. Device can now be safely removed." << std::endl;

        return EXIT_SUCCESS;
    }
    catch (opera_error_t & error)
    {
        std::cerr << "<=== Opera ===>\n" << error.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (std::exception & error)
    {
        std::cerr << "<=== std::exception ===>\n" << error.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "[ERROR]: Unknown exception caught" << std::endl;
        return EXIT_FAILURE;
    }
}

